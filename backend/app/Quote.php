<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    const ACCEPT  = 1;
    const DECLINE = 2;
    const REVISE  = 3;

    public function user()
    {
        return $this->belongsTo( 'App\User', 'created_by' );
    }

    public function category()
    {
        return $this->belongsTo( 'App\Category' );
    }

    public function client()
    {
        return $this->belongsTo( 'App\Client' );
    }

    public function search( $request )
    {
        $quotes    = new Quote();
        $page_size = 10;

        if ( $request->id ) {
            $quotes = $quotes->where( 'id', '=', $request->id );
        }


        if ( $request->title ) {
            $quotes = $quotes->where( 'title',
                                      'like', '%' . $request->title . '%' );
        }

        if ( $request->contact ) {
            $quotes = $quotes->where( 'contact',
                                      'like', '%' . $request->contact . '%' );
        }

        if ( $request->status ) {
            $quotes = $quotes->where( 'status',
                                      '=', $request->status );
        }


        // client table query
        if ( $request->client_name ) {
            $quotes = $quotes->whereHas( 'client', function ( $query ) use ( $request ) {
                $query->where( 'name', 'LIKE', '%' . $request->client_name . '%' );
            } );
        }

        // user table query
        if ( $request->user_name ) {
            $quotes = $quotes->whereHas( 'user', function ( $query ) use ( $request ) {
                $query->where( 'name', 'LIKE', '%' . $request->user_name . '%' );
            } );
        }

        // category table query
        if ( $request->category_name ) {
            $quotes = $quotes->whereHas( 'category', function ( $query ) use ( $request ) {
                $query->where( 'name', 'LIKE', '%' . $request->category_name . '%' );
            } );
        }


        # global search
        if ( $request->global_search ) {

            if ( !$request->client_id ) {
                $quotes = $quotes->where( 'id', '=', $request->global_search );

                $quotes = $quotes->orwhere( 'title',
                                            'like', '%' . $request->global_search . '%' );


                $quotes = $quotes->orwhere( 'contact',
                                            'like', '%' . $request->global_search . '%' );

            }
            // client table query
            /*if ( !$request->client_id ) {
                $quotes = $quotes->orwhereHas( 'client', function ( $query ) use ( $request ) {
                    $query->where( 'name', 'LIKE', '%' . $request->global_search . '%' );
                } );
            }


            // user table query
            if ( !$request->client_id ) {
                $quotes = $quotes->orwhereHas( 'user', function ( $query ) use ( $request ) {
                    $query->where( 'name', 'LIKE', '%' . $request->global_search . '%' );
                } );
            }

            // category table query
            if ( !$request->client_id ) {
                $quotes = $quotes->orwhereHas( 'category', function ( $query ) use ( $request ) {
                    $query->where( 'name', 'LIKE', '%' . $request->global_search . '%' );
                } );
            }*/
        }


        if ( $request->client_id ) {
            $quotes = $quotes->where( 'client_id',
                                      '=', $request->client_id );
        }


        // order by
        if ( $request->name_asc ) { # for job dropdown on timesheet form
            $quotes = $quotes->orderBy( 'title', 'asc' );

        } else {
            $quotes = $quotes->orderBy( 'title', 'asc' );
        }


        // Page size
        if ( $request->page_size ) {
            $page_size = $request->page_size;
        }


        $quotes = $quotes->with( [ 'client', 'user', 'category' ] )->paginate( $page_size );

        return $quotes;

    }

    protected $fillable = [
        'title', 'client_id', 'description', 'contact', 'category_id', 'status', 'created_by'
    ];


    //Make custom attribute available in the json response
    protected $appends = [ 'number' ];


    // define a mutator for the field "category_id"
    public function setCategoryIdAttribute( $value )
    {
        $this->attributes[ 'category_id' ] = (int)$value;
    }


    // define a get mutator for the field "project_no"
    public function getNumberAttribute()
    {
        return 'Q-' . $this->id;
    }
}
