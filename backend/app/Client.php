<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = [
        'name',
        'address_line_1',
        'address_line_2',
        'state',
        'postcode',
        'phone',
        'payable_invoice_due_date',
        'receivable_invoice_due_date',
    ];

    protected $dates = [ 'payable_invoice_due_date', 'receivable_invoice_due_date' ];

    public function getDates()
    {
        return [ 'created_at', 'updated_at', 'payable_invoice_due_date', 'receivable_invoice_due_date' ];
    }

    public function setPayableInvoiceDueDateAttribute( $value )
    {
        if ( strlen( $value ) ) {
            $this->attributes[ 'payable_invoice_due_date' ] = date( 'Y-m-d', strtotime( $value ) );
        } else {
            $this->attributes[ 'payable_invoice_due_date' ] = '0001-01-10';
        }
    }

    public function setReceivableInvoiceDueDateAttribute( $value )
    {
        if ( strlen( $value ) ) {
            $this->attributes[ 'receivable_invoice_due_date' ] = date( 'Y-m-d', strtotime( $value ) );
        } else {
            $this->attributes[ 'receivable_invoice_due_date' ] = '0001-01-10';
        }
    }

    Public function getPayableInvoiceDueDateAttribute( $value )
    {
        return strtotime( $value ) > 0 ? date( 'd-m-Y', strtotime( $value ) ) : '';
    }

    Public function getReceivableInvoiceDueDateAttribute( $value )
    {
        return strtotime( $value ) > 0 ? date( 'd-m-Y', strtotime( $value ) ) : '';
        //return date( 'd-m-Y', strtotime( $value ) );
    }

    Public function getStartDateAttribute( $value )
    {
        return date( 'd-m-Y', strtotime( $value ) );
        //return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
        //return \Carbon\Carbon::($value)->format('d/m/Y');
        //return Carbon\Carbon::createFromFormat('d-m-Y',$value);
    }


    public function search( $request )
    {
        $clients       = new Client();
        $page_dropdown = Client::dropdown_pagesize();
        $page_size     = array_values( $page_dropdown )[ 0 ];

        if ( $request->id ) {
            $clients = $clients->where( 'id', '=', $request->id );
        }
        if ( $request->name ) {
            $clients = $clients->where( 'name',
                                        'like', '%' . $request->name . '%' );
        }
        if ( $request->state ) {
            $clients = $clients->where( 'state',
                                        'like', '%' . $request->state . '%' );
        }
        if ( $request->postcode ) {
            $clients = $clients->where( 'postcode',
                                        'like', '%' . $request->postcode . '%' );
        }
        if ( $request->phone ) {
            $clients = $clients->where( 'phone',
                                        'like', '%' . $request->phone . '%' );
        }
        if ( $request->address_line_1 ) {
            $clients = $clients->where( 'address_line_1',
                                        'like', '%' . $request->address_line_1 . '%' );
        }

        # global search
        if ( $request->global_search ) {
            $clients = $clients->where( 'id', '=', $request->global_search );

            $clients = $clients->orwhere( 'name',
                                          'like', '%' . $request->global_search . '%' );

            $clients = $clients->orwhere( 'state',
                                          'like', '%' . $request->global_search . '%' );

            $clients = $clients->orwhere( 'postcode',
                                          'like', '%' . $request->global_search . '%' );

            $clients = $clients->orwhere( 'phone',
                                          'like', '%' . $request->global_search . '%' );

            $clients = $clients->orwhere( 'address_line_1',
                                          'like', '%' . $request->global_search . '%' );

        }

        if ( $request->name_asc ) { # for given sort param
            $clients = $clients->orderBy( 'name', 'asc' );

        } else {
            $clients = $clients->orderBy( 'name', 'asc' );
        }


        if ( $request->page_size ) {
            $page_size = $request->page_size;
        }


        $clients = $clients->paginate( $page_size );

        return $clients;

    }

    public static function dropdown_pagesize()
    {
        return [
            '10', '25', '50', '100'
        ];

    }

}

