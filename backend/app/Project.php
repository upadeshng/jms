<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $fillable = [
        'title', 'client_id', 'description', 'category_id', 'order_no', 'quoted_sell_price', 'created_by'
    ];


    //Make custom attribute available in the json response
    protected $appends = [ 'number','quoted_sell_price_formatted' ];

    Public function getQuotedSellPriceFormattedAttribute(  )
    {
        return '$' . number_format( $this->quoted_sell_price, 2 );
    }


    public function user()
    {
        return $this->belongsTo( 'App\User', 'created_by' );
    }

    public function category()
    {
        return $this->belongsTo( 'App\Category' );
    }

    public function client()
    {
        return $this->belongsTo( 'App\Client' );
    }

    public function search( $request )
    {
        $projects  = new Project();
        $page_size = 10;

        if ( $request->id ) {
            $projects = $projects->where( 'id', '=', $request->id );
        }


        if ( $request->title ) {
            $projects = $projects->where( 'title',
                                          'like', '%' . $request->title . '%' );
        }

        if ( $request->order_no ) {
            $projects = $projects->where( 'order_no',
                                          'like', '%' . $request->order_no . '%' );
        }

        if ( $request->quoted_sell_price ) {
            $projects = $projects->where( 'quoted_sell_price',
                                          'like', '%' . $request->quoted_sell_price . '%' );
        }


        // client table query
        if ( $request->client_name ) {
            $projects = $projects->whereHas( 'client', function ( $query ) use ( $request ) {
                $query->where( 'name', 'LIKE', '%' . $request->client_name . '%' );
            } );
        }

        // user table query
        if ( $request->user_name ) {
            $projects = $projects->whereHas( 'user', function ( $query ) use ( $request ) {
                $query->where( 'name', 'LIKE', '%' . $request->user_name . '%' );
            } );
        }


        # global search
        if ( $request->global_search ) {
            $projects = $projects->where( 'id', '=', $request->global_search );

            $projects = $projects->orwhere( 'title',
                                            'like', '%' . $request->global_search . '%' );


            $projects = $projects->orwhere( 'order_no',
                                            'like', '%' . $request->global_search . '%' );

            // client table query
            if ( !$request->client_id ) {
                $projects = $projects->orwhereHas( 'client', function ( $query ) use ( $request ) {
                    $query->where( 'name', 'LIKE', '%' . $request->global_search . '%' );
                } );
            }


            // user table query
            if ( !$request->client_id ) {
                $projects = $projects->orwhereHas( 'user', function ( $query ) use ( $request ) {
                    $query->where( 'name', 'LIKE', '%' . $request->global_search . '%' );
                } );
            }
        }


        if ( $request->client_id ) {
            $projects = $projects->where( 'client_id',
                                          '=', $request->client_id );
        }


        // order by
        if ( $request->name_asc ) { # for job dropdown on timesheet form
            $projects = $projects->orderBy( 'title', 'asc' );

        } else {
            $projects = $projects->orderBy( 'title', 'asc' );
        }


        // Page size
        if ( $request->page_size ) {
            $page_size = $request->page_size;
        }


        $projects = $projects->with( [ 'client', 'user' ] )->paginate( $page_size );

        return $projects;

    }


    // define a mutator for the field "category_id"
    public function setCategoryIdAttribute( $value )
    {
        $this->attributes[ 'category_id' ] = (int)$value;
    }

    // define a mutator for the field "quoted_sell_price"
    public function setQuotedSellPriceAttribute( $value )
    {
        $ex   = explode( ',', $value );
        $data = implode( '', $ex );
        $ex   = explode( '$', $data );
        $data = implode( '', $ex );

        $this->attributes[ 'quoted_sell_price' ] = (int)$data;
    }


    // define a get mutator for the field "project_no"
    public function getNumberAttribute()
    {
        return 'P-' . $this->id;
    }


}
