<?php

namespace App\Http\Controllers;

use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $id = null )
    {
        if ( $id == null ) {

            $clients = new Client();
            $data    = $clients->search( $request );

            return response()->json( [ 'clients' => $data ], 200 );


            /*return response()
                ->json( [ 'page_dropdown' => $data[ 'page_dropdown' ],
                          'jobs'          => $data[ 'jobs' ],
                        ] );*/

        } else {
            return $this->show( $id );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        if ( $request->isMethod( 'put' ) ) {
            //Get the task
            $client = Client::find( $request->id );
            if ( !$client ) {
                return $this->response->errorNotFound( 'Job Not Found' );
            }
        } else {
            $client = new Client();
        }


        /* Note: from text editor if only posting html character without content like <p></p> <br>; then it should return as blank so that validation works*/
        /*$description = $request->input('description');
        $description = strip_tags($description) ? $description : '';
        $request->request->set('description', $description);*/

        /*-- Validation --*/
        // WE DO VALIDATION IN ANGULAR FRONTEND
        /*$validator = \Illuminate\Support\Facades\Validator::make( $request->all(), Client::rules() );
        if ( $validator->fails() ) {
            return response()->json( [ 'errors' => $validator->messages() ] );
        }*/


        $client->name                        = $request->input( 'name' );
        $client->address_line_1              = $request->input( 'address_line_1', '' );
        $client->address_line_2              = $request->input( 'address_line_2', '' );
        $client->state                       = $request->input( 'state', '' );
        $client->postcode                    = $request->input( 'postcode', '' );
        $client->phone                       = $request->input( 'phone', '' );
        $client->email                       = $request->input( 'email', '' );
        $client->payable_invoice_due_date    = $request->input( 'payable_invoice_due_date' );
        $client->receivable_invoice_due_date = $request->input( 'receivable_invoice_due_date' );
        $client->payment_terms               = $request->input( 'payment_terms' );

        /*$date = Carbon::parse($request->input('payable_invoice_due_date'))->format('Y-m-d');
        $client->payable_invoice_due_date    = '0000-00-00';

        $date = Carbon::parse($request->input('receivable_invoice_due_date'))->format('Y-m-d');
        $client->receivable_invoice_due_date    = '0000-00-00';*/

        /*$dt = Carbon::createFromFormat('m/d/Y', strtotime($request->input('payable_invoice_due_date')));
        $client->payable_invoice_due_date    = $dt->format('Y-m-d');

        $dt = Carbon::createFromFormat('m/d/Y', strtotime($request->input('receivable_invoice_due_date')));
        $client->receivable_invoice_due_date    = $dt->format('Y-m-d');*/

        /*$client->payable_invoice_due_date    = Carbon::createFromFormat('Y-m-d', $request->input( 'payable_invoice_due_date',0));
        $client->receivable_invoice_due_date    = Carbon::createFromFormat('Y-m-d', $request->input( 'receivable_invoice_due_date',0));*/
        //$client->receivable_invoice_due_date = $request->input( 'receivable_invoice_due_date', '' );
        if ( $client->save() ) {
            return response()->json( [ 'client' => $this->show( $client->id ) ], 200 );
            //return $this->show( $client->id );
        }
        return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
        //return 'Job record successfully created with id' . $client->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //return Client::with( 'user' )->where( 'id', $id )->first();
        return Client::find( $id );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function update( Request $request, $id )
    {
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $client = Job::find( $id )->delete();
        return 'Job record successfully deleted';
    }
}
