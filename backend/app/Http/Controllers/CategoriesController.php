<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $id = null )
    {
        if ( $id == null ) {

            $categories = new Category();
            $data       = $categories->search( $request );

            return response()->json( [ 'categories' => $data ], 200 );

        } else {
            return $this->show( $id );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        if ( $request->isMethod( 'put' ) ) {
            $category = Category::find( $request->id );
            if ( !$category ) {
                return $this->response->errorNotFound( 'Category Not Found' );
            }
        } else {
            $category = new Category();
        }

        $category->name       = $request->input( 'name' );
        $category->created_by = $request->input( 'created_by', '' );
        if ( $category->save() ) {
            return response()->json( [ 'Category' => $this->show( $category->id ) ], 200 );
        }
        return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        return Category::find( $id );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }
}
