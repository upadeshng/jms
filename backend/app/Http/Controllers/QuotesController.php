<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;

class QuotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $id = null )
    {
        if ( $id == null ) {

            $quotes = new Quote();
            $data   = $quotes->search( $request );

            return response()->json( [ 'quotes' => $data ], 200 );

        } else {
            return $this->show( $id );
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        if ( $request->isMethod( 'put' ) ) {
            $quote = Quote::find( $request->id );
            if ( !$quote ) {
                return $this->response->errorNotFound( 'Quote Not Found' );
            }
        } else {
            $quote = new Quote();
        }

        $quote->title       = $request->input( 'title' );
        $quote->client_id   = $request->input( 'client_id', '' );
        $quote->description = $request->input( 'description', '' );
        $quote->contact     = $request->input( 'contact', '' );
        $quote->category_id = $request->input( 'category_id' );
        $quote->status      = $request->input( 'status', 0 );
        $quote->created_by  = $request->input( 'created_by', '' );
        if ( $quote->save() ) {
            return response()->json( [ 'quote' => $this->show( $quote->id ) ], 200 );
        }
        return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        return Quote::with( [ 'client', 'user', 'category' ] )->where( 'id', $id )->first();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }
}
