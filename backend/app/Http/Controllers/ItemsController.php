<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $id = null )
    {
        if ( $id == null ) {

            $items = new Item();
            $data  = $items->search( $request );

            return response()->json( [ 'items' => $data ], 200 );

        } else {
            return $this->show( $id );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        if ( $request->isMethod( 'put' ) ) {
            //Get the task
            $item = Item::find( $request->id );
            if ( !$item ) {
                return $this->response->errorNotFound( 'Item Not Found' );
            }
        } else {
            $item = new Item();
        }


        $item->name       = $request->input( 'name' );
        $item->cost_price = $request->input( 'cost_price', '' );
        $item->sell_price = $request->input( 'sell_price', '' );
        $item->quantity   = $request->input( 'quantity', '' );
        $item->created_by = $request->input( 'created_by', '' );

        if ( $item->save() ) {
            return response()->json( [ 'item' => $this->show( $item->id ) ], 200 );
        }
        return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
        //return 'Job record successfully created with id' . $item->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //return Item::with( 'user' )->where( 'id', $id )->first();
        return Item::find( $id );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function update( Request $request, $id )
    {
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $item = Item::find( $id )->delete();
        return 'Item record successfully deleted';
    }
}
