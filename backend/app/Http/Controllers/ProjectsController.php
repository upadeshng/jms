<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $id = null )
    {
        if ( $id == null ) {

            $projects = new Project();
            $data     = $projects->search( $request );

            return response()->json( [ 'projects' => $data ], 200 );

        } else {
            return $this->show( $id );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        if ( $request->isMethod( 'put' ) ) {
            $project = Project::find( $request->id );
            if ( !$project ) {
                return $this->response->errorNotFound( 'Project Not Found' );
            }
        } else {
            $project = new Project();
        }

        $project->title             = $request->input( 'title' );
        $project->client_id         = $request->input( 'client_id', '' );
        $project->description       = $request->input( 'description', '' );
        $project->category_id       = $request->input( 'category_id' );
        $project->order_no          = $request->input( 'order_no', '' );
        $project->quoted_sell_price = $request->input( 'quoted_sell_price', '' );
        $project->created_by        = $request->input( 'created_by', '' );
        if ( $project->save() ) {
            return response()->json( [ 'project' => $this->show( $project->id ) ], 200 );
        }
        return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        return Project::with( [ 'client', 'user', 'category' ] )->where( 'id', $id )->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }
}
