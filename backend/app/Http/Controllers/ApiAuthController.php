<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;

class ApiAuthController extends Controller
{

    public function authenticate( Request $request )
    {
        //header("Access-Control-Allow-Origin: *");
        $data = [ 'token' => 'aaaavvvvccc' ];
        //return json_encode($data);

        //return response()->json(['token' => 'aaaavvvvccc'], 200);

        $postInput   = file_get_contents( 'php://input' );
        $credentials = json_decode( $postInput, true );

        //get user data
        //$credentials = request()->only('email', 'password');
        //$credentials = $request->only('email', 'password');

        //return $_POST;
        //check if user credentials are correct
        try {
            $token = JWTAuth::attempt( $credentials );

            if ( !$token ) {
                return response()->json( [ 'error' => 'invalid_credentials' ], 401 );
            }

        } catch ( JWTException $e ) {

            return response()->json( [ 'error' => 'something_went_wrong' ], 500 );
        }

        //generate token

        //return ['token' => $token];
        return response()->json( [ 'auth_token'     => $token,
                                   'auth_user' => Auth::User() ], 200 );
    }

    public function register()
    {

        //get user data
        $name     = request()->name;
        $email    = request()->email;
        $password = request()->password;

        //register user
        User::create( [
                          'name'     => $name,
                          'email'    => $email,
                          'password' => bcrypt( $password ),
                      ] );

        //generate token
        $token = JWTAuth::fromUser( $user );

        return response()->json( [ 'token' => $token ], 200 );
    }

    public function refresh()
    {
        return [ 'success' => 'true' ];
    }
}
