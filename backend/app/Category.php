<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    protected $fillable = [
        'name','created_by'
    ];

    public function search( $request )
    {
        $categories  = new Category();
        $page_size = 10;

        if ( $request->id ) {
            $categories = $categories->where( 'id', '=', $request->id );
        }
        if ( $request->name ) {
            $categories = $categories->where( 'name',
                                          'like', '%' . $request->name . '%' );
        }



        # global search
        if ( $request->global_search ) {
            $categories = $categories->where( 'id', '=', $request->global_search );

            $categories = $categories->orwhere( 'name',
                                            'like', '%' . $request->global_search . '%' );

        }

        if ( $request->name_asc ) { # for category dropdown on timesheet form
            $categories = $categories->orderBy( 'name', 'asc' );

        } else {
            $categories = $categories->orderBy( 'name', 'asc' );
        }


        if ( $request->page_size ) {
            $page_size = $request->page_size;
        }


        $categories = $categories->paginate( $page_size );

        return $categories;

    }

}
