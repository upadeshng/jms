<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name',
        'cost_price',
        'sell_price',
        'quantity',
        'created_by',
    ];

    //Make custom attribute available in the json response
    protected $appends = [ 'total_cost', 'cost_price_formatted' ];

    Public function getCostPriceFormattedAttribute()
    {
        return '$' . number_format( $this->cost_price, 2 );
    }

    Public function getTotalCostAttribute()
    {
        $total = $this->quantity * $this->cost_price;
        return '$' . number_format( $total, 2 );
    }

    Public function setCostPriceAttribute( $value )
    {
        $ex   = explode( ',', $value );
        $data = implode( '', $ex );
        $ex   = explode( '$', $data );
        $data = implode( '', $ex );

        $this->attributes[ 'cost_price' ] = (int)$data;
    }

    Public function setSellPriceAttribute( $value )
    {
        $ex   = explode( ',', $value );
        $data = implode( '', $ex );
        $ex   = explode( '$', $data );
        $data = implode( '', $ex );

        $this->attributes[ 'sell_price' ] = (int)$data;
    }

    Public function setQuantityAttribute( $value )
    {
        $this->attributes[ 'quantity' ] = (double)$value;
    }


    public function search( $request )
    {
        $items     = new Item();
        $page_size = 10;

        if ( $request->id ) {
            $items = $items->where( 'id', '=', $request->id );
        }
        if ( $request->name ) {
            $items = $items->where( 'name',
                                    'like', '%' . $request->name . '%' );
        }

        # global search
        if ( $request->global_search ) {
            $items = $items->orwhere( 'name',
                                      'like', '%' . $request->global_search . '%' );


        }

        if ( $request->name_asc ) { # for given sort param
            $items = $items->orderBy( 'name', 'asc' );

        } else {
            $items = $items->orderBy( 'name', 'asc' );
        }


        if ( $request->page_size ) {
            $page_size = $request->page_size;
        }


        $items = $items->paginate( $page_size );

        return $items;

    }

}
