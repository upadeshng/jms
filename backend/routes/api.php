<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::group(['middleware' => 'cors'], function () {

Route::get( '/user', function ( Request $request ) {
    return $request->user();
} )->middleware( 'jwt.auth' );//->middleware('auth:api');


Route::post( '/authenticate', [
    'uses' => 'ApiAuthController@authenticate',
] );


/* Clients */
Route::get( '/clients/{id?}', 'ClientsController@index' )->middleware( 'jwt.auth' );
Route::post( '/clients', 'ClientsController@store' )->middleware( 'jwt.auth' );
Route::put( '/clients/{id?}', 'ClientsController@store' )->middleware( 'jwt.auth' );
Route::delete( '/clients/{id}', 'ClientsController@destroy' )->middleware( 'jwt.auth' );

/* Projects */
Route::get( '/projects/{id?}', 'ProjectsController@index' )->middleware( 'jwt.auth' );
Route::post( '/projects', 'ProjectsController@store' )->middleware( 'jwt.auth' );
Route::put( '/projects/{id?}', 'ProjectsController@store' )->middleware( 'jwt.auth' );
Route::delete( '/projects/{id}', 'ProjectsController@destroy' )->middleware( 'jwt.auth' );


/* Categories */
Route::get( '/categories/{id?}', 'CategoriesController@index' )->middleware( 'jwt.auth' );
Route::post( '/categories', 'CategoriesController@store' )->middleware( 'jwt.auth' );
Route::put( '/categories/{id?}', 'CategoriesController@store' )->middleware( 'jwt.auth' );
Route::delete( '/categories/{id}', 'CategoriesController@destroy' )->middleware( 'jwt.auth' );


/* Quotes */
Route::get( '/quotes/{id?}', 'QuotesController@index' )->middleware( 'jwt.auth' );
Route::post( '/quotes', 'QuotesController@store' )->middleware( 'jwt.auth' );
Route::put( '/quotes/{id?}', 'QuotesController@store' )->middleware( 'jwt.auth' );
Route::delete( '/quotes/{id}', 'QuotesController@destroy' )->middleware( 'jwt.auth' );


/* Items */
Route::get( '/items/{id?}', 'ItemsController@index' )->middleware( 'jwt.auth' );
Route::post( '/items', 'ItemsController@store' )->middleware( 'jwt.auth' );
Route::put( '/items/{id?}', 'ItemsController@store' )->middleware( 'jwt.auth' );
Route::delete( '/items/{id}', 'ItemsController@destroy' )->middleware( 'jwt.auth' );


