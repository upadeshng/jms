<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEquipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'equipments', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'name' );
            $table->double( 'quantity' );
            $table->double( 'total_cost' );
            $table->integer( 'created_by' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'equipments' );
    }
}
