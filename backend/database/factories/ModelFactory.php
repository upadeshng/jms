<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define( App\User::class, function ( Faker\Generator $faker ) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt( 'secret' ),
        'remember_token' => str_random( 10 ),
    ];
} );


// Clients
$factory->define( App\Client::class, function ( Faker\Generator $faker ) {

    return [
        'name'                        => $faker->company,
        'address_line_1'              => $faker->streetAddress,
        'address_line_2'              => $faker->streetAddress,
        'state'                       => $faker->word,
        'postcode'                    => $faker->postcode,
        'phone'                       => $faker->numberBetween( 411111111, 499999999 ),
        'email'                       => $faker->companyEmail,
        'payable_invoice_due_date'    => $faker->date( 'Y-m-d' ),
        'receivable_invoice_due_date' => $faker->date( 'Y-m-d' ),
    ];
} );

// Projects
$factory->define( App\Project::class, function ( Faker\Generator $faker ) {

    return [
        'client_id'         => $faker->numberBetween( 1, 100 ),
        'title'             => $faker->sentence( 2 ),
        'description'       => $faker->sentence( 200 ),
        'category_id'       => $faker->numberBetween( 1, 100 ),
        'order_no'          => $faker->word,
        'quoted_sell_price' => $faker->numberBetween( 1000, 20000 ),
        'created_by'        => $faker->numberBetween( 1, 100 ),
    ];
} );

// Categories
$factory->define( App\Category::class, function ( Faker\Generator $faker ) {

    return [
        'name'       => $faker->sentence( 1 ),
        'created_by' => $faker->numberBetween( 1, 100 ),
    ];
} );

// Quotes
$factory->define( App\Quote::class, function ( Faker\Generator $faker ) {

    return [
        'client_id'   => $faker->numberBetween( 1, 100 ),
        'title'       => $faker->sentence( 2 ),
        'description' => $faker->sentence( 200 ),
        'category_id' => $faker->numberBetween( 1, 100 ),
        'contact'     => $faker->name,
        'status'      => $faker->numberBetween( 1, 3 ),
        'created_by'  => $faker->numberBetween( 1, 100 ),
    ];
} );

// Items
$factory->define( App\Item::class, function ( Faker\Generator $faker ) {

    return [
        'name'       => $faker->sentence( 2 ),
        'quantity'   => $faker->numberBetween( 5, 80 ),
        'cost_price' => $faker->numberBetween( 100, 1000 ),
        'sell_price' => $faker->numberBetween( 100, 1000 ),
        'created_by' => $faker->numberBetween( 1, 100 ),
    ];
} );