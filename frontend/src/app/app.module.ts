import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule, Http, CookieXSRFStrategy } from '@angular/http';

/* import modules */
import {UsersModule} from './users/users.module';
import {ClientsModule} from './clients/clients.module';
import {ProjectsModule} from './projects/projects.module';
import {QuotesModule} from './quotes/quotes.module';
import {ItemsModule} from './items/items.module';

/* Routing Module */
import {AppRoutingModule} from './app-routing.module';
import {LoginRoutingModule} from './login/login-routing.module';
import {NavComponent} from './nav/nav.component';
import {FooterComponent} from './footer/footer.component';

/* import Component */
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {HomeComponent} from './home/home.component';
import {AuthService} from "./auth.service";
import {AuthGuard} from "./auth-guard.service";
import {AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth} from 'angular2-jwt';
import { JwtConfigService, JwtHttp } from 'angular2-jwt-refresh';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';


export function getAuthHttp(http) {
    return new AuthHttp(new AuthConfig({
        headerName: 'Authorization',
        headerPrefix: 'bearer',
        tokenName: 'token',
        tokenGetter: (() => JSON.parse(localStorage.getItem('auth'))['token']),
        globalHeaders: [{'Accept': 'application/json'}],
        noJwtError: true
    }), http);
}

@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        FooterComponent,
        LoginComponent,
        LogoutComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        UsersModule,
        ClientsModule,
        ProjectsModule,
        QuotesModule,
        ItemsModule,
        SimpleNotificationsModule,
        LoginRoutingModule,
        AppRoutingModule,
    ],
    providers: [/*AuthService,*/ AuthGuard,
        AuthService,
        {
            provide: AuthHttp,
            useFactory: getAuthHttp,
            deps: [Http]
        },
        {provide: LocationStrategy, useClass: HashLocationStrategy}
        /*provideAuth({
            headerName: 'Authorization',
            headerPrefix: 'bearer',
            tokenName: 'token',
            tokenGetter: (() => JSON.parse(localStorage.getItem('auth'))['token']),
            globalHeaders: [{'Content-Type': 'application/json'}],
            noJwtError: true
        })*/],
    bootstrap: [AppComponent]
})
export class AppModule {
}



export function getXSRF() {
    return new CookieXSRFStrategy('csrftoken', 'X-CSRFToken');
}

