import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from "./auth.service";
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate{

    constructor(
        private _authService: AuthService,
        private _router: Router
    ){

    }
    canActivate(){
    
    
        //if (this._authService.isLoggedIn){
        if (tokenNotExpired()) {
            console.log('ATUH GUARD SAYD THEY ARE ALREADY LOGGED IN!');
            return true;
        }
        
    
        this._router.navigate(['logout']);
        return false;
        //if (!this._authService.is_logged_id()) {
        /*if (tokenNotExpired()) {
            return true;
        }

        console.log('token expired so logging out..');
        this._router.navigate(['login']);*/

      

    }
}