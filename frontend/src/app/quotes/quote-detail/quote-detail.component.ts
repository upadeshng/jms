import { Component, OnInit } from '@angular/core';
import { QuoteService } from "../shared/quote.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-quote-detail',
  templateUrl: './quote-detail.component.html',
  styleUrls: ['./quote-detail.component.css']
})
export class QuoteDetailComponent implements OnInit {
  
  public quote: string;
  public loading = false;
  
  constructor(private _quoteService: QuoteService,
              private _route: ActivatedRoute) { }
  
  ngOnInit() {
    
    this._load_quote();
  }
  
  private _load_quote(){
    this._route.params.subscribe(params => {
      
      let id = Number.parseInt(params['id']);
      //console.log('getting quote with id: ', id);
      
      this._quoteService.get(id)
		  .map(res => res.json())
		  .subscribe(
              data => {
                this.quote = data;
                console.log('client is ', data.client.name);
                console.log(data);
              },
              err => {
                console.log(err);
              },
              () => {
                this.loading = false;
                console.log('Request Complete')
              }
          );
      
    });
  }

}
