import { Injectable } from '@angular/core';
import { AuthHttp } from "angular2-jwt";
import { URLS } from "../../urls";

@Injectable()
export class QuoteService {
  api_url: string = URLS.base_api() + 'quotes';
  
  constructor( private _authHttp: AuthHttp ) { }
  
  getall( params = null ) {
    return this._authHttp.get( this.api_url + '?' + params );
  }
  
  get(id: number) {
    return this._authHttp.get(this.api_url + '/' + id);
  }
  
  create( form_fields ) {
    
    return this._authHttp.post( this.api_url, form_fields );
    
  }
  update(formFields, id) {
    
    return this._authHttp.put(this.api_url + '/' + id, formFields);
    
  }
  delete(id: number) {
    return this._authHttp.delete('/api/users/' + id);
  }
}
