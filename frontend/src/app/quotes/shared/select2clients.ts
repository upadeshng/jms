import { URLS } from '../../urls';

//declare var jQuery: any;
declare var $: any;

export class Select2clients {
	
	// https://embed.plnkr.co/gHZKfFbTuTJY8lX5BG7o/
	constructor( private id: string ) {
		$( id ).select2( {
			width             : '100%',
			ajax              : {
				//url: 'https://api.github.com/search/repositories',
				url           : URLS.base_api() + 'clients',
				headers       : {
					"Authorization": "Bearer " + JSON.parse( localStorage.getItem( 'auth' ) )[ 'token' ]
				},
				datatype      : 'json',
				delay         : 250,
				data          : function ( params: any ) {
					return {
						name     : params.term,
						page_size: 50
					};
				},
				processResults: function ( data: any, params: any ) {
					
					/*
					 // Remember on live hosting put JSON.stringify...*/
					let hostname = document.location.hostname;
					let obj      = null;
					if ( hostname == 'localhost' )
						obj = JSON.parse( data );
					else
						obj = JSON.parse( JSON.stringify( data || null ) );
					
					let items = obj.clients.data;
					
					return {
						
						results: $.map( items, function ( client ) {
								console.log( client );
								
								return {
									id  : client.id,
									text: client.name
								};
							}
						)
					};
					
				},
				cache         : true
			},
			placeHolder       : 'Search...',
			minimumInputLength: 0
		} )
	}
	
	/*getSelectedValues( id: string ) {
	 return $( id ).val();
	 }*/
}