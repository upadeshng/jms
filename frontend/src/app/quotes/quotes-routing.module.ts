import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import component */
import {HomeComponent} from '../home/home.component';
import {QuotesComponent} from './quotes.component';
import {QuoteListComponent} from './quote-list/quote-list.component';
import {QuoteCreateComponent} from './quote-create/quote-create.component';
import {QuoteDetailComponent} from './quote-detail/quote-detail.component';
import {QuoteEditComponent} from './quote-edit/quote-edit.component';
import {AuthGuard} from "../auth-guard.service";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard],
        children: [
            {path: 'quotes', component: QuoteListComponent},
            {path: 'quotes/create', component: QuoteCreateComponent},
            {path: 'quotes/:id', component: QuoteDetailComponent},
            {path: 'quotes/edit/:id', component: QuoteEditComponent},
        ]

    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class QuotesRoutingModule {
}

//export const routedComponents = [QuotesComponent];