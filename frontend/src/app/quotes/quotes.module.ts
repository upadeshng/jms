import { NgModule } from '@angular/core';

/* import module */
import { CommonModule } from '@angular/common';
import { QuotesRoutingModule } from './quotes-routing.module';

/* import component */
import { QuotesComponent } from './quotes.component';
import { QuoteListComponent } from './quote-list/quote-list.component';
import { QuoteCreateComponent } from './quote-create/quote-create.component';
import { QuoteDetailComponent } from './quote-detail/quote-detail.component';
import { QuoteEditComponent } from './quote-edit/quote-edit.component';
import { AuthHttp } from "angular2-jwt";
import { QuoteService } from "./shared/quote.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule( {
	imports     : [
		CommonModule,
		QuotesRoutingModule,
		FormsModule,
		ReactiveFormsModule
	],
	declarations: [ QuotesComponent, QuoteListComponent, QuoteCreateComponent, QuoteDetailComponent, QuoteEditComponent ],
	providers   : [
		QuoteService,
		AuthHttp,
	],
} )
export class QuotesModule {
}
