import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { QuoteService } from "../shared/quote.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import 'rxjs/add/operator/first';
import { Select2clients } from "../shared/select2clients";
import { Select2categories } from "../shared/select2categories";

// to use for jquery
declare var $: any;

@Component( {
	selector   : 'app-quote-create',
	templateUrl: './quote-create.component.html',
	styleUrls  : [ './quote-create.component.css' ]
} )
export class QuoteCreateComponent implements OnInit {
	
	public quote_form: FormGroup;
	public clients: string;
	public client: string;
	public set_client_id: string   = '';
	public set_category_id: string = '';
	
	private _select2Clients: Select2clients;
	private _select2Categories: Select2categories;
	
	constructor( private _quoteService: QuoteService,
				 private _fb: FormBuilder,
				 private _router: Router,
				 private _route: ActivatedRoute,
				 private _location: Location,
				 private _el_ref: ElementRef ) {
		
		this.quote_form = _fb.group( {
			client_id  : [ '', Validators.required ],
			title      : [ '', Validators.required ],
			description: [ '' ],
			contact    : [ '' ],
			category_id: [ '0' ],
			status     : [ '0' ],
			created_by : [ JSON.parse( localStorage.getItem( 'auth' ) )[ 'user' ][ 'id' ] ],
		} );
	}
	
	ngOnInit() {
		
		// Initialize select2
		this._select2Clients    = new Select2clients( "#select2clients" );
		this._select2Categories = new Select2categories( "#select2categories" );
		
	}
	
	on_create( e ): void {
		//console.log(this.quote_form.value);
		this._quoteService.create( this.quote_form.value )
			.map( res => res.json() )
			.subscribe(
				data => {
					
					this._router.navigate( [ 'quotes/' + data.quote.id ] );
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	ngAfterViewInit() {
		
		
		// set client_id changed value from select2 plugin
		$( this._el_ref.nativeElement ).find( '#select2clients' ).on( 'change', ( e: any ) => {
			
			this.set_client_id = e.target.value;
			
		} );
		
		// set category_id changed value from select2 plugin
		$( this._el_ref.nativeElement ).find( '#select2categories' ).on( 'change', ( e: any ) => {
			
			this.set_category_id = e.target.value;
			
		} );
		
	}
}
