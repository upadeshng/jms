import { Component, OnInit, ElementRef } from '@angular/core';
import { QuoteService } from "../shared/quote.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Select2clients } from "../shared/select2clients";
import { Select2categories } from "../shared/select2categories";
import { Location } from "@angular/common";
import { URLS } from "../../urls";

declare var $: any;

@Component({
  selector: 'app-quote-edit',
  templateUrl: './quote-edit.component.html',
  styleUrls: ['./quote-edit.component.css']
})
export class QuoteEditComponent implements OnInit {
  
  public quote: string;
  public loading = false;
  public quote_form: FormGroup;
  public set_client_id: string   = '';
  public set_category_id: string = '';
  
  private _select2Clients: Select2clients;
  private _select2Categories: Select2categories;
    
    
    
    constructor( private _quoteService: QuoteService,
                 private _fb: FormBuilder,
                 private _router: Router,
                 private _route: ActivatedRoute,
                 private _location: Location,
                 private _el_ref: ElementRef ) {
        
        this.quote_form = _fb.group( {
            client_id  : [ '', Validators.required ],
            title      : [ '', Validators.required ],
            description: [ '' ],
            contact    : [ '' ],
            category_id: [ '0' ],
            status     : [ '0' ],
            created_by : [ JSON.parse( localStorage.getItem( 'auth' ) )[ 'user' ][ 'id' ] ],
        } );
    }
  
  ngOnInit() {
    
    this._load_quote();
  }
  
  private _load_quote(){
    this._route.params.subscribe(params => {
      
      let id = Number.parseInt(params['id']);
      //console.log('getting quote with id: ', id);
      
      this._quoteService.get(id)
		  .map(res => res.json())
		  .subscribe(
              data => {
                this.quote = data;
                console.log('client is ', data.client.name);
                console.log(data);
    
                  this.quote_form = this._fb.group( {
                      client_id  : [ data.client_id, Validators.required ],
                      title      : [ data.title, Validators.required ],
                      description: [ data.description ],
                      contact    : [ data.contact ],
                      category_id: [ data.category_id ],
                      status     : [ data.status ],
                      created_by : [ data.created_by ],
                  } );
    
    
                  this._select2Clients    = new Select2clients( "#select2clients");
                  this._select2Categories = new Select2categories( "#select2categories" );
    
                  // Set select2 value & name after project loaded
                  // - temporary re-initializing ajax in select2 without reusing Select2clients
                  $( function () {
                      $( '#select2clients' ).select2( {
                          ajax              : {
                              url           : URLS.base_api() + 'clients',
                              headers       : {
                                  "Authorization": "Bearer " + JSON.parse( localStorage.getItem( 'auth' ) )[ 'token' ]
                              },
                              datatype      : 'json',
                              delay         : 250,
                              data          : function ( params: any ) {
                                  return {
                                      name     : params.term,
                                      page_size: 100
                                  };
                              },
                              processResults: function ( data: any, params: any ) {
                    
                                  let obj   = JSON.parse( data );
                                  let items = obj.clients.data;
                    
                                  return {
                        
                                      results: $.map( items, function ( client ) {
                                              console.log( client );
                                
                                              return {
                                                  id  : client.id,
                                                  text: client.name
                                              };
                                          }
                                      )
                                  };
                    
                              },
                              cache         : true
                          },
                          initSelection: function ( element, callback ) {
                              callback( { 'id': data.client.id, 'text': data.client.name } );
                          }
                      } );
        
                      // Set select2 value & name after project loaded
                      // Temporary re-initializing ajax in select2 without reusing Select2clients
                      $( '#select2categories' ).select2( {
                          ajax : {
                              //url: 'https://api.github.com/search/repositories',
                              url           : URLS.base_api() + 'categories',
                              headers       : {
                                  "Authorization": "Bearer " + JSON.parse( localStorage.getItem( 'auth' ) )[ 'token' ]
                              },
                              datatype      : 'json',
                              delay         : 250,
                              data          : function ( params: any ) {
                                  return {
                                      name     : params.term,
                                      page_size: 100
                                  };
                              },
                              processResults: function ( data: any, params: any ) {
                    
                                  let obj   = JSON.parse( data );
                                  let items = obj.categories.data;
                    
                                  return {
                        
                                      results: $.map( items, function ( category ) {
                                              console.log( category );
                                
                                              return {
                                                  id  : category.id,
                                                  text: category.name
                                              };
                                          }
                                      )
                                  };
                    
                              },
                              cache         : true
                          },
                          initSelection: function ( element, callback ) {
                              if(data.client && data.category) {
                                  callback( { 'id': data.category.id, 'text': data.category.name } );
                              }else{
                                  callback( { 'id': 0, 'text': '- Select Category -' } );
                    
                              }
                          }
                      } );
        
                  } );
                  
              },
              err => {
                console.log(err);
              },
              () => {
                this.loading = false;
                console.log('Request Complete')
              }
          );
      
    });
  }
    
    on_update( e ): void {
        let id = Number.parseInt( this._route.params[ 'value' ][ 'id' ] );
        
        this._quoteService.update( this.quote_form.value, id )
			.map( res => res.json() )
			.subscribe(
                data => {
                    this._router.navigate( [ '/quotes/' + id ] );
                },
                err => {
                    console.log( err );
                },
                () => console.log( 'Request Complete' )
            );
    }
    
    ngAfterViewInit() {
        
        
        // set client_id changed value from select2 plugin
        $( this._el_ref.nativeElement ).find( '#select2clients' ).on( 'change', ( e: any ) => {
            
            this.set_client_id = e.target.value;
            
        } );
        
        // set category_id changed value from select2 plugin
        $( this._el_ref.nativeElement ).find( '#select2categories' ).on( 'change', ( e: any ) => {
            
            this.set_category_id = e.target.value;
            
        } );
        
    }
}
