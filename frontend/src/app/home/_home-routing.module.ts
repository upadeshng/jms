import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import components */
import {HomeComponent} from './home.component';
import {ClientsComponent} from '../clients/clients.component';

const routes: Routes = [
    {path: 'clients', component: ClientsComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomeRoutingModule {
}

//export const routedComponents = [ClientsComponent];