import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";
import {URLS} from '../urls';

declare var $: any;

@Component( {
	selector   : 'app-home',
	templateUrl: './home.component.html',
	styleUrls  : [ './home.component.css' ]
} )
export class HomeComponent implements OnInit {
	
	
	constructor( private _authService: AuthService,
				 private _router: Router ) {
		//_authService.login( "username", "password" );
	}
	
	ngOnInit() {
		
		
		// root page && already logged in, then redirect to home page
		if ( this._authService.should_go_home() ) {
			this._router.navigate( [ URLS.home() ] );
		}
		
		console.log( this._router.url );
		
		console.log( 'home component' );
	}
	
	ngAfterViewInit() {
		
		$( function () {
			
			$('body').removeAttr('class');
			$('body').addClass('hold-transition skin-blue layout-top-nav');
			console.log('jquery');
		} );
		
		
		
	}
	
}
