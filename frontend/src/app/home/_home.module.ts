import {NgModule} from '@angular/core';

/* import modules */
import {CommonModule} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth } from 'angular2-jwt';

/* import Component */

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        FormsModule,
        ReactiveFormsModule,

    ],
    //declarations: [HomeComponent, ClientListComponent, ClientCreateComponent],
    providers: [
        AuthHttp,
        provideAuth({
            headerName: 'Authorization',
            headerPrefix: 'bearer',
            tokenName: 'token',
            tokenGetter: (() => localStorage.getItem('id_token')),
            globalHeaders: [{ 'Content-Type': 'application/json' }],
            noJwtError: true
        })
    ],
})
export class HomeModule {
}
