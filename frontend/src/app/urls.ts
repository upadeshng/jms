import { DOCUMENT} from '@angular/platform-browser';

export class URLS {
    static base(): string {
        let hostname = document.location.hostname;

        if(hostname == 'localhost')
            return 'http://jms.local:8888';
        else
            return 'http://upadeshbhatta.com/jms/backend/public';
    
    }
    
    static home(): string{
        return '/clients';
    }
    
    static base_api(): string{
        let hostname = document.location.hostname;
        
        if(hostname == 'localhost')
            return 'http://jms.local:8888/api/';
        else
            return 'http://upadeshbhatta.com/jms/backend/public/api/';
    }
}