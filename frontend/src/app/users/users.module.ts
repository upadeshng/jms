import {NgModule} from '@angular/core';

/* import modules */
import {CommonModule} from '@angular/common';
import {UsersRoutingModule}   from './users-routing.module';

/* import components */
import {UsersComponent} from './users.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserCreateComponent} from './user-create/user-create.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UserUpdateComponent} from './user-update/user-update.component';


@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule
    ],
    declarations: [UserListComponent, UserCreateComponent, UsersComponent, UserDetailComponent, UserUpdateComponent]
})
export class UsersModule {

}
