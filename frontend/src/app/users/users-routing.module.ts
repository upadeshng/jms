import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import components */
import {HomeComponent} from '../home/home.component';
import {UsersComponent} from './users.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserCreateComponent} from './user-create/user-create.component';
import {AuthGuard} from "../auth-guard.service";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [ AuthGuard ],
        children: [
            {path: 'users', component: UserListComponent},
            {path: 'users/create', component: UserCreateComponent}
            //{ path: ':id', component: HeroDetailComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {
}

