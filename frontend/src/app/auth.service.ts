import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { URLS } from './urls';
import { Observable } from "rxjs";
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Router } from "@angular/router";

@Injectable()
export class AuthService {
	
	public token: string;
		   isLoggedIn = false;
		   loading    = true;
	
	constructor( private _http: Http,
				 private _router: Router ) {
		// set token if saved in local storage
		let auth   = JSON.parse( localStorage.getItem( 'auth' ) );
		this.token = auth && auth.token;
		
		//console.log(JSON.parse(localStorage.getItem('auth'))['token']);
	}
	
	login( username: string, password: string ): Observable<boolean> {
		
		return this._http.post(
				URLS.base_api() + 'authenticate', JSON.stringify( { email: username, password: password } ) )
			.map( ( response: Response ) => {
				// login successful if there's a jwt token in the response
				
				let auth_token = response.json() && response.json().auth_token;
				let auth_user  = response.json() && response.json().auth_user;
				
				if ( auth_token ) {
					
					this.isLoggedIn = true;
					
					// set token property
					this.token = auth_token;
					
					// store username and jwt token in local storage to keep user logged in between page refreshes
					localStorage.setItem(
						'auth',
						JSON.stringify( { token: auth_token, user: auth_user } ) );
					
					// set id_token for tokenNotExpired() functionality
					localStorage.setItem( 'id_token', auth_token );
					
					// return true to indicate successful login
					return true;
					
				} else {
					
					// return false to indicate failed login
					return false;
				}
			} );
		
	}
	
	logout() {
		this.isLoggedIn = false;
		
		localStorage.removeItem( 'auth' );
		localStorage.removeItem( 'id_token' );
		
		//this._router.navigate(['login']);
	}
	
	is_logged_in() {
		
		if ( localStorage.getItem( 'auth' )/* && tokenNotExpired()*/ ) {
			return true;
		}
		
		//return false;
	}
	
	should_go_home() {
		//console.log(this._router.url);
		
		// already logged in BUT current in root page, then redirect to home page
		if ( this.is_logged_in() && this._router.url == '/' ) return true;
		
		// current in lo page && already logged in, then redirect to home page
		else if ( this.is_logged_in() && this._router.url == '/login' ) return true;
		
		else return false;
	}
}