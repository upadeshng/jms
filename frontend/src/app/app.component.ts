import { Component, OnInit } from '@angular/core';
import { AuthService } from "./auth.service";
import { Location } from '@angular/common';
import { Router } from "@angular/router";

@Component( {
	selector   : 'app-root',
	templateUrl: './app.component.html',
	styleUrls  : [ './app.component.css' ]
} )
export class AppComponent implements OnInit{
	title        = 'app works!';
	is_logged_in = false;
	home_url: string = 'clients/create';
	
	constructor( private _authService: AuthService,
				 private _location: Location,
				 private _router: Router) {
		
		//authService.login("username", "password");
	}
	
	ngOnInit() {
		
	}
	
	back_clicked() {
		this._location.back();
	}
	
	
	
	
}
