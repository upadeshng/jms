import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ProjectService } from "../shared/project.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { ClientService } from "../../clients/shared/client.service";
import 'rxjs/add/operator/first';
import { Select2clients } from '../shared/select2clients'
import { Select2categories } from '../shared/select2categories'

// to use for jquery
declare var $: any;

@Component( {
	selector   : 'app-project-create',
	templateUrl: './project-create.component.html',
	styleUrls  : [ './project-create.component.css' ]
} )
export class ProjectCreateComponent implements OnInit {
	public project_form: FormGroup;
	public clients: string;
	public client: string;
	public set_client_id: string   = '';
	public set_category_id: string = '';
	
	private _select2Clients: Select2clients;
	private _select2Categories: Select2categories;
	
	constructor( private _projectService: ProjectService,
				 private _clientService: ClientService,
				 private _fb: FormBuilder,
				 private _router: Router,
				 private _route: ActivatedRoute,
				 private _location: Location,
				 private _el_ref: ElementRef ) {
		
		this.project_form = _fb.group( {
			client_id        : [ '', Validators.required ],
			title            : [ '', Validators.required ],
			description      : [ '' ],
			category_id      : [ '0' ],
			order_no         : [ '' ],
			quoted_sell_price: [ '0' ],
			created_by       : [ JSON.parse( localStorage.getItem( 'auth' ) )[ 'user' ][ 'id' ] ],
		} );
		
	}
	
	ngOnInit() {
		
		// Initialize select2
		this._select2Clients    = new Select2clients( "#select2clients" );
		this._select2Categories = new Select2categories( "#select2categories" );

	}
	
	on_create( e ): void {
		//console.log(this.project_form.value);
		this._projectService.create( this.project_form.value )
			.map( res => res.json() )
			.subscribe(
				data => {
					
					this._router.navigate( [ 'projects/' + data.project.id ] );
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	go_back() {
		this._location.back();
	}
	
	ngAfterViewInit() {
		
		
		// set client_id changed value from select2 plugin
		$( this._el_ref.nativeElement ).find( '#select2clients' ).on( 'change', ( e: any ) => {
			
			this.set_client_id = e.target.value;
			
		} );
		
		// set category_id changed value from select2 plugin
		$( this._el_ref.nativeElement ).find( '#select2categories' ).on( 'change', ( e: any ) => {
			
			this.set_category_id = e.target.value;
			
		} );
		
	}
	
}
