import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import component */
import {HomeComponent} from '../home/home.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectCreateComponent} from './project-create/project-create.component';
import {ProjectDetailComponent} from './project-detail/project-detail.component';
import {ProjectEditComponent} from './project-edit/project-edit.component';
import {AuthGuard} from "../auth-guard.service";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard],
        children: [
            {path: 'projects', component: ProjectListComponent},
            {path: 'projects/create', component: ProjectCreateComponent},
            /*{path: 'projects/create/:id', component: ProjectCreateComponent},*/ //creating id as c
            {path: 'projects/:id', component: ProjectDetailComponent},
            {path: 'projects/edit/:id', component: ProjectEditComponent},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProjectsRoutingModule {
}

//export const routedComponents = [ProjectsComponent];