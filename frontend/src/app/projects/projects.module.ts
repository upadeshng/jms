import { NgModule } from '@angular/core';

/* import module */
import { CommonModule } from '@angular/common';
import { ProjectsRoutingModule } from './projects-routing.module';

/* import component */
import { ProjectsComponent } from './projects.component';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthHttp } from "angular2-jwt";
import { QuoteService } from "../quotes/shared/quote.service";

@NgModule( {
	imports     : [
		CommonModule,
		ProjectsRoutingModule,
		FormsModule,
		ReactiveFormsModule
	],
	
	declarations: [ ProjectsComponent, ProjectCreateComponent, ProjectListComponent, ProjectEditComponent, ProjectDetailComponent ],
	
	providers   : [
		QuoteService,
		AuthHttp,
	],
} )
export class ProjectsModule {
}
