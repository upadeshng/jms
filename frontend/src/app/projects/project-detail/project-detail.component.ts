import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from "@angular/router";
import { ProjectService } from "../shared/project.service";

@Component( {
	selector   : 'app-project-detail',
	templateUrl: './project-detail.component.html',
	styleUrls  : [ './project-detail.component.css' ]
} )
export class ProjectDetailComponent implements OnInit {
	
	public project: string;
	public loading = false;
	
	constructor(private _projectService: ProjectService,
				private _route: ActivatedRoute) { }
	
	ngOnInit() {
		
		this._load_project();
	}
	
	private _load_project(){
		this._route.params.subscribe(params => {
			
			let id = Number.parseInt(params['id']);
			console.log('getting project with id: ', id);
			
			this._projectService.get(id)
				.map(res => res.json())
				.subscribe(
					data => {
						this.project = data;
						console.log(data);
					},
					err => {
						console.log(err);
					},
					() => {
						this.loading = false;
						console.log('Request Complete')
					}
				);
			
		});
	}
}
