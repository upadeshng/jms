import { Component, OnInit, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ItemService } from "../shared/item.service";
import { ActivatedRoute, Router } from "@angular/router";

declare var $: any;

@Component( {
	selector   : 'app-item-edit',
	templateUrl: './item-edit.component.html',
	styleUrls  : [ './item-edit.component.css' ]
} )
export class ItemEditComponent implements OnInit {
	
	itemForm: FormGroup;
	item: string;
	loading = false;
	
	constructor( private _itemService: ItemService,
				 private _fb: FormBuilder,
				 private _route: ActivatedRoute,
				 private _router: Router ) {
		
	}
	
	ngOnInit() {
		
		
		
		// show loading message
		this.loading = true;
		
		// get item data
		
		this._load_item();
		
	}
	
	private _load_item() {
		this._route.params.subscribe( params => {
			
			let id = Number.parseInt( params[ 'id' ] );
			console.log( 'getting item with id: ', id );
			
			this._itemService.get( id )
				.map( res => res.json() )
				.subscribe(
					data => {
						this.item = data;
						
						console.log( this.item );
						
						this.itemForm = this._fb.group( {
							name      : [ data.name, Validators.required ],
							cost_price: [ data.cost_price ],
							quantity  : [ data.quantity ],
							created_by: [ data.created_by ],
						} );
						
					},
					err => {
						console.log( err );
					},
					() => {
						this.loading = false;
						console.log( 'Request Complete' )
					}
				);
			
		} );
	}
	
	on_update( e ): void {
		let id = Number.parseInt( this._route.params[ 'value' ][ 'id' ] );
		
		this._itemService.update( this.itemForm.value, id )
			.map( res => res.json() )
			.subscribe(
				data => {
					this._router.navigate( [ '/items/' + id ] );
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	ngAfterViewInit() {
		
	}
	
}
