import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ItemService } from "../shared/item.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component( {
	selector   : 'app-item-create',
	templateUrl: './item-create.component.html',
	styleUrls  : [ './item-create.component.css' ]
} )
export class ItemCreateComponent implements OnInit {
	
	itemForm: FormGroup;
	item: string;
	loading = false;
	
	constructor( private _itemService: ItemService,
				 private _fb: FormBuilder,
				 private _route: ActivatedRoute,
				 private _router: Router ) {
		
		this.itemForm = this._fb.group( {
			name      : [ '', Validators.required ],
			cost_price: [ '' ],
			quantity  : [ '' ],
			created_by: [ JSON.parse( localStorage.getItem( 'auth' ) )[ 'user' ][ 'id' ] ],
		} );
		
	}
	
	ngOnInit() {
		
		
		
		// show loading message
		this.loading = true;
		
		// get item data
		
	}
	
	on_create( e ): void {
		
		this._itemService.create( this.itemForm.value )
			.map( res => res.json() )
			.subscribe(
				data => {
					this._router.navigate( [ '/items/' + data.item.id ] );
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	ngAfterViewInit() {
		
	}
	
}
