import { Component, OnInit } from '@angular/core';
import { ItemService } from "../shared/item.service";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  
  
  item: string;
  loading = false;
  
  constructor( private _itemService: ItemService,
               private _fb: FormBuilder,
               private _route: ActivatedRoute,
               private _router: Router ) {
    
  }
  
  ngOnInit() {
    
    
    
    // show loading message
    this.loading = true;
    
    // get item data
    
    this._load_item();
    
  }
  
  private _load_item() {
    this._route.params.subscribe( params => {
      
      let id = Number.parseInt( params[ 'id' ] );
      console.log( 'getting item with id: ', id );
      
      this._itemService.get( id )
		  .map( res => res.json() )
		  .subscribe(
              data => {
                this.item = data;
                
                console.log( this.item );
                
              },
              err => {
                console.log( err );
              },
              () => {
                this.loading = false;
                console.log( 'Request Complete' )
              }
          );
      
    } );
  }
  
  
  ngAfterViewInit() {
    
  }
  
  
}
