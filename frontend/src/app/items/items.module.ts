import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemCreateComponent } from './item-create/item-create.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { ItemEditComponent } from './item-edit/item-edit.component';
import { FormBuilder, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AuthHttp } from "angular2-jwt";
import { ItemsRoutingModule } from "./items-routing.module";
import { ItemService } from "./shared/item.service";

@NgModule( {
	imports     : [
		CommonModule,
		ItemsRoutingModule,
		FormsModule,
		ReactiveFormsModule
	
	],
	declarations: [ ItemListComponent, ItemCreateComponent, ItemDetailComponent, ItemEditComponent ],
	providers   : [
		ItemService,
		AuthHttp ]
} )
export class ItemsModule {
}
