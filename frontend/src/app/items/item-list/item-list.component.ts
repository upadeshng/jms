import { Component, OnInit } from '@angular/core';
import { ItemService } from "../shared/item.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  
  items: string;
  loading = false;
  pages = [];
  param = [];
  
  constructor(private _itemService: ItemService,
              private _route: Router) {
  }
  
  generate_params(key, value) {
    let arr = this.param;
    let newArr = [];
    
    for (var i = 0, l = arr.length; i < l; i++) {
      if (!arr[i][key]) {
        newArr.push(arr[i]);
      }
    }
    
    var obj = {};
    obj[key] = value;
    newArr.push(obj);
    
    this.param = newArr;
  }
  
  serialise_object(obj) {
    var pairs = [];
    for (var prop in obj) {
      if (!obj.hasOwnProperty(prop)) {
        continue;
      }
      if (Object.prototype.toString.call(obj[prop]) == '[object Object]') {
        //console.log(obj[prop].aa);
        if (!obj[prop].key) {
          //pairs.push(this.serialiseObject(obj[prop]));
          pairs.push(this.serialise_object(obj[prop]));
        }
        continue;
      }
      pairs.push(prop + '=' + obj[prop]);
    }
    return pairs.join('&');
  }
  
  
  pagination_set_param(key, value) {
    this.generate_params(key, value);
    let serialized_param = this.serialise_object(this.param);
    this.load_all(serialized_param);
    console.log(serialized_param);
  }
  
  
  load_all(params = null) {
    this._itemService.getall(params)
		.map(res => res.json())
		.subscribe(
            data => {
              this.items = data;
              console.log(data);
              
              // prevent blank page, if on last page & page_size > data no
              if(!data.items.from && data.items.current_page > 1){
                this.pagination_set_param('page',data.items.last_page);
              }
              // make pages array in order to use loop ngFor in html
              let new_pages_arr = [];
              for (let i = 0; i < data.items.last_page; i++) {
                new_pages_arr.push(i);
              }
              
              this.pages = new_pages_arr;
              //this.items.items. = new_pages_arr;
              
            },
            err => {
              console.log(err);
            },
            () => {
              this.loading = false;
              console.log('Request Complete')
            }
        );
  }
  
  ngOnInit() {
    
    // show loading message
    this.loading = true;
    
    // get all items
    this.load_all();
    
  }

}
