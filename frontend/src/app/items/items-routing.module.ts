import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* import components */
import { HomeComponent } from '../home/home.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemCreateComponent } from './item-create/item-create.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { ItemEditComponent } from './item-edit/item-edit.component';
import { AuthGuard } from "../auth-guard.service";

const routes: Routes = [
	{
		path       : '',
		component  : HomeComponent,
		canActivate: [ AuthGuard ],
		children   : [
			{ path: 'items', component: ItemListComponent, canActivate: [ AuthGuard ] },
			{ path: 'items/create', component: ItemCreateComponent, canActivate: [ AuthGuard ] },
			{ path: 'items/:id', component: ItemDetailComponent, canActivate: [ AuthGuard ] },
			{ path: 'items/edit/:id', component: ItemEditComponent, canActivate: [ AuthGuard ] },
		]
	},
];

@NgModule( {
	imports: [ RouterModule.forChild( routes ) ],
	exports: [ RouterModule ],
} )
export class ItemsRoutingModule {
}

//export const routedComponents = [ItemsComponent];