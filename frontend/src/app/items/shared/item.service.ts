import { Injectable } from '@angular/core';
import { AuthHttp } from "angular2-jwt";
import { URLS } from "../../urls";

@Injectable()
export class ItemService {
  api_url: string = URLS.base_api() + 'items';

  constructor(private _authHttp: AuthHttp) { }
  
  get( id: number ) {
    return this._authHttp.get( this.api_url + '/' + id );
  }
  
  getall( params = null ) {
    return this._authHttp.get( this.api_url + '?' + params );
    
  }
  
  create( formFields ) {
    
    return this._authHttp.post( this.api_url, formFields );
    
  }
  
  update( formFields, id ) {
    
    return this._authHttp.put( this.api_url + '/' + id, formFields );
    
  }
  
  delete( id: number ) {
    return this._authHttp.delete( '/api/users/' + id );
  }
}
