import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import components */
import {LoginComponent} from './login.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LoginRoutingModule {
}

//export const routedComponents = [ClientsComponent];