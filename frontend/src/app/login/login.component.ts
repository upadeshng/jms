import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from "../auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import 'rxjs/add/operator/map';
import { Response } from "@angular/http";
import { URLS } from '../urls';

declare var $: any;
@Component( {
	selector   : 'app-login',
	templateUrl: './login.component.html',
	styleUrls  : [ './login.component.css' ],
} )

export class LoginComponent implements OnInit, OnDestroy {

// we are logging in
	invalid_login = false;
	
	loggingIn = true;
	
	returnUrl: string;
	// reset error message
	error_message = '';
	
	loginForm: FormGroup;
	
	constructor( private _authService: AuthService,
				 private _route: ActivatedRoute,
				 private _router: Router,
				 private fb: FormBuilder ) {
		
		this.loginForm = fb.group( {
			username: [ 'jayden.rolfson@example.net', Validators.required ],
			password: [ 'secret', Validators.required ],
		} );
	}
	
	on_login() {
		
		this._authService.login( this.loginForm.value[ 'username' ],
			this.loginForm.value[ 'password' ] )
			.subscribe( result => {
				
				if ( result === true ) {
					//this._router.navigate([this.returnUrl]);
					this._router.navigate( [ URLS.home() ] );
				} else {
					this.error_message = 'Username or password is incorrect';
					//this.loading = false;
				}
			} );
		
	}
	
	ngOnInit() {
		
		// root page && already logged in, then redirect to home page
		if ( this._authService.should_go_home() ) {
			this._router.navigate( [ URLS.home() ] );
		}
		
		// reset login status, as token may be expired and already redirected back to login
		//this._authService.logout();
		
		// get return url from route parameters or default to '/'
		this.returnUrl = this._route.snapshot.queryParams[ 'returnUrl' ] || '/';
		
	}
	
	ngOnDestroy() {
	}
	
	ngAfterViewInit() {
		
		$( function () {
			
			$('body').removeAttr('class');
			$('body').addClass('hold-transition login-page');
			console.log('jquery');
		} );
		
		
		
	}
	
}
