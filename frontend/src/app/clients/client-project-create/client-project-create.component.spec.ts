import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientProjectCreateComponent } from './client-project-create.component';

describe('ClientProjectCreateComponent', () => {
  let component: ClientProjectCreateComponent;
  let fixture: ComponentFixture<ClientProjectCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientProjectCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientProjectCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
