import { Component, OnInit } from '@angular/core';
import { ProjectService } from "../../projects/shared/project.service";
import { ActivatedRoute } from "@angular/router";
import { ClientService } from "../shared/client.service";

@Component( {
	selector   : 'app-client-project-list',
	templateUrl: './client-project-list.component.html',
	styleUrls  : [ './client-project-list.component.css' ]
} )
export class ClientProjectListComponent implements OnInit {
	
	client: string;
	projects: string;
	pages = [];
	param = [];
	
	constructor( private _projectService: ProjectService,
				 private _clientService: ClientService,
				 private _route: ActivatedRoute ) { }
	
	private _get_client(){
		this._route.params.subscribe(url_params => {
			
			let id = Number.parseInt(url_params['id']);
			console.log('getting client with id: ', id);
			
			this._clientService.get(id)
				.map(res => res.json())
				.subscribe(
					data => {
						this.client = data;
						console.log(data);
					},
					err => {
						console.log(err);
					},
					() => {
						//this.loading = false;
						console.log('Request Complete')
					}
				);
			
		});
	}
	
	private _get_all( params = null ) {
		this._route.params.subscribe( url_params => {
			
			let id = Number.parseInt( url_params[ 'id' ] );
			console.log( 'getting client with id: ', id );
			
			this._projectService.getall( params + '&client_id=' + id )
				.map( res => res.json() )
				.subscribe(
					data => {
						this.projects = data;
						console.log( data );
						// prevent blank page, if on last page & page_size > data no
						if ( !data.projects.from && data.projects.current_page > 1 ) {
							this.pagination_set_param( 'page', data.projects.last_page );
						}
						// make pages array in order to use loop ngFor in html
						let new_pages_arr = [];
						for ( let i = 0; i < data.projects.last_page; i++ ) {
							new_pages_arr.push( i );
						}
						
						this.pages = new_pages_arr;
						
					},
					err => {
						console.log( err );
					},
					() => {
						console.log( 'Request Complete' )
					}
				);
		} );
	}
	
	generate_params( key, value ) {
		let arr    = this.param;
		let newArr = [];
		
		for ( var i = 0, l = arr.length; i < l; i++ ) {
			if ( !arr[ i ][ key ] ) {
				newArr.push( arr[ i ] );
			}
		}
		
		var obj    = {};
		obj[ key ] = value;
		newArr.push( obj );
		
		this.param = newArr;
	}
	
	serialise_object( obj ) {
		var pairs = [];
		for ( var prop in obj ) {
			if ( !obj.hasOwnProperty( prop ) ) {
				continue;
			}
			if ( Object.prototype.toString.call( obj[ prop ] ) == '[object Object]' ) {
				//console.log(obj[prop].aa);
				if ( !obj[ prop ].key ) {
					//pairs.push(this.serialiseObject(obj[prop]));
					pairs.push( this.serialise_object( obj[ prop ] ) );
				}
				continue;
			}
			pairs.push( prop + '=' + obj[ prop ] );
		}
		return pairs.join( '&' );
	}
	
	pagination_set_param( key, value ) {
		this.generate_params( key, value );
		let serialized_param = this.serialise_object( this.param );
		this._get_all( serialized_param );
		console.log( serialized_param );
	}
	
	ngOnInit() {
		// get all projects
		this._get_all();
		
		// get client
		this._get_client();
		
	}
	
}
