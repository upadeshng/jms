import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientProjectListComponent } from './client-project-list.component';

describe('ClientProjectListComponent', () => {
  let component: ClientProjectListComponent;
  let fixture: ComponentFixture<ClientProjectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientProjectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientProjectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
