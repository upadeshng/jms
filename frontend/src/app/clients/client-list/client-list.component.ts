import { Component, OnInit, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { Router } from "@angular/router";
import { ClientService } from "../shared/client.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CustomValidators } from "../../shared/custom-validators";
import { NotificationsService } from "angular2-notifications";

declare var $: any;

@Component( {
	selector   : 'app-client-list',
	templateUrl: './client-list.component.html',
	styleUrls  : [ './client-list.component.css' ],
	animations : [
		
		trigger( 'move_panel', [
			/*transition('void => *', [
			 animate(900, keyframes([
			 style({'opacity':0}),
			 style({'opacity':1}),
			 ]))
			 ]),*/
			/*transition('void => *',[
			 style({transform: 'translateX(-100%)'}),
			 animate(400)
			 ])*/
		] ),
	]
} )
export class ClientListComponent implements OnInit {
	
	clients: string;
	loading        = false;
	pages          = [];
	param          = [];
	clientForm: FormGroup;
	public options = {
		position    : [ "bottom", "left" ],
		timeOut     : 90000005000,
		lastOnBottom: true
	};
	
	constructor( private _clientService: ClientService,
				 private _route: Router,
				 private _fb: FormBuilder,
				 private _router: Router,
				 private _notificationsService: NotificationsService ) {
		
		this.set_form( _fb );
	}
	
	generate_params( key, value ) {
		let arr    = this.param;
		let newArr = [];
		
		for ( var i = 0, l = arr.length; i < l; i++ ) {
			if ( !arr[ i ][ key ] ) {
				newArr.push( arr[ i ] );
			}
		}
		
		var obj    = {};
		obj[ key ] = value;
		newArr.push( obj );
		
		this.param = newArr;
	}
	
	serialise_object( obj ) {
		var pairs = [];
		for ( var prop in obj ) {
			if ( !obj.hasOwnProperty( prop ) ) {
				continue;
			}
			if ( Object.prototype.toString.call( obj[ prop ] ) == '[object Object]' ) {
				//console.log(obj[prop].aa);
				if ( !obj[ prop ].key ) {
					//pairs.push(this.serialiseObject(obj[prop]));
					pairs.push( this.serialise_object( obj[ prop ] ) );
				}
				continue;
			}
			pairs.push( prop + '=' + obj[ prop ] );
		}
		return pairs.join( '&' );
	}
	
	pagination_set_param( key, value ) {
		this.generate_params( key, value );
		let serialized_param = this.serialise_object( this.param );
		this.load_all( serialized_param );
		console.log( serialized_param );
	}
	
	load_all( params = null ) {
		this._clientService.getall( params )
			.map( res => res.json() )
			.subscribe(
				data => {
					this.clients = data;
					console.log( data );
					
					// prevent blank page, if on last page & page_size > data no
					if ( !data.clients.from && data.clients.current_page > 1 ) {
						this.pagination_set_param( 'page', data.clients.last_page );
					}
					// make pages array in order to use loop ngFor in html
					let new_pages_arr = [];
					for ( let i = 0; i < data.clients.last_page; i++ ) {
						new_pages_arr.push( i );
					}
					
					this.pages = new_pages_arr;
					//this.clients.clients. = new_pages_arr;
					
				},
				err => {
					console.log( err );
				},
				() => {
					this.loading = false;
					console.log( 'Request Complete' )
				}
			);
	}
	
	ngOnInit() {
		
		// show loading message
		this.loading = true;
		
		// get all clients
		this.load_all();
		
	}
	
	set_form( _fb ) {
		this.clientForm = _fb.group( {
			name                       : [ '', Validators.required ],
			address_line_1             : [ '' ],
			address_line_2             : [ '' ],
			state                      : [ '' ],
			postcode                   : [ '' ],
			phone                      : [ '' ],
			email                      : [ '',
				Validators.compose( [ CustomValidators.emailValidatorWithoutRequired ] ) ],
			payable_invoice_due_date   : [ '' ],
			receivable_invoice_due_date: [ '' ],
			payment_terms              : [ '' ],
		} );
	}
	
	on_create( e ): void {
		this._clientService.create( this.clientForm.value )
			.map( res => res.json() )
			.subscribe(
				data => {
					
					// get all clients
					this.load_all();
					
					// reset form values
					this.set_form( this._fb );
					
					this._notificationsService.success(
						'Success!!',
						'Client "' + data.client.name + '"  added successfully.',
						{
							timeOut        : 10000,
							showProgressBar: true,
							pauseOnHover   : true,
							clickToClose   : false,
							maxLength      : 60,
							position       : [ 'top', 'right' ]
						}
					);
					
					$( function () {
						// close dialog modal
						$( "#modal_create_client" ).modal( "hide" );
						
					} );
					
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
}
