import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientQuoteListComponent } from './client-quote-list.component';

describe('ClientQuoteListComponent', () => {
  let component: ClientQuoteListComponent;
  let fixture: ComponentFixture<ClientQuoteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientQuoteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientQuoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
