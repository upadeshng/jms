import {Component, OnInit} from '@angular/core';
import {ClientService} from "../shared/client.service";
import {ActivatedRoute} from '@angular/router';
import { ProjectService } from "../../projects/shared/project.service";
import { QuoteService } from "../../quotes/shared/quote.service";

@Component({
    selector: 'app-client-detail',
    templateUrl: './client-detail.component.html',
    styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {

    client: string;
    loading = false;
    id: number = 0;
    project_count: number = 0;
    quote_count: number = 0;

    constructor(private _clientService: ClientService,
                private _projectService: ProjectService,
                private _quoteService: QuoteService,
                private _route: ActivatedRoute) {

    }

    ngOnInit() {

        // show loading message
        this.loading = true;

        // get client data
        this._get_client();
        
        // get project count
        this._get_project_count();
        
        // get quote count
        this._get_quote_count();

    }
    
    private _get_project_count(){
        
        this._projectService.getall('client_id='+this.id)
			.map(res => res.json())
			.subscribe(
                data => {
                    let all_data = data.projects.data;
                    this.project_count = all_data.length;
                }
            );
    }
    
    private _get_quote_count(){
        
        this._quoteService.getall('client_id='+this.id)
			.map(res => res.json())
			.subscribe(
                data => {
                    let all_data = data.quotes.data;
                    this.quote_count = all_data.length;
                }
            );
    }
    private _get_client(){
        this._route.params.subscribe(params => {
        
            let id = Number.parseInt(params['id']);
            this.id = id;
            console.log('getting client with id: ', id);
        
            this._clientService.get(id)
				.map(res => res.json())
				.subscribe(
                    data => {
                        this.client = data;
                        console.log(data);
                    },
                    err => {
                        console.log(err);
                    },
                    () => {
                        this.loading = false;
                        console.log('Request Complete')
                    }
                );
        
        });
    }
}
