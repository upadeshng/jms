import { Component, OnInit, ElementRef, trigger, transition, style, animate } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ClientService } from '../shared/client.service';
import 'rxjs/add/operator/map';
import { Router } from "@angular/router";
import { CustomValidators } from '../../shared/custom-validators';
import { Location } from '@angular/common';

// to use for jquery
declare var $: any;

@Component( {
	selector   : 'app-client-create',
	templateUrl: './client-create.component.html',
	styleUrls  : [ './client-create.component.css' ],
	animations:[
		trigger('move_panel',[
			transition('void => *',[
				style({transform: 'translateX(-100%)'}),
				animate(400)
			])
		])
	]
} )
export class ClientCreateComponent implements OnInit {
	
	clientForm: FormGroup;
	set_payable_invoice_due_date: string    = '';
	set_receivable_invoice_due_date: string = '';
	
	constructor( private _clientService: ClientService,
				 private _fb: FormBuilder,
				 private _router: Router,
				 private _location: Location,
				 private _el_ref: ElementRef ) {
		this.clientForm = _fb.group( {
			name                       : [ '', Validators.required ],
			address_line_1             : [ '' ],
			address_line_2             : [ '' ],
			state                      : [ '' ],
			postcode                   : [ '' ],
			phone                      : [ '' ],
			email                      : [ '',
				Validators.compose( [ CustomValidators.emailValidatorWithoutRequired ] ) ],
			payable_invoice_due_date   : [ '' ],
			receivable_invoice_due_date: [ '' ],
			payment_terms: [ '' ],
		} );
		
	}
	
	ngOnInit() {
		
	}
	
	on_create( e ): void {
		this._clientService.create( this.clientForm.value )
			.map( res => res.json() )
			.subscribe(
				data => {
					
					// redirect to client detail page
					this._router.navigate( [ 'clients/' + data.client.id ] );
					//this._router.navigate(['clients/12']);
					
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	go_back() {
		this._location.back();
	}
	
	ngAfterViewInit() {
		
		$( function () {
			$( "#datepicker-payable" ).datepicker( {
				format: "dd-mm-yyyy",
			} );
			
			$( "#datepicker-receivable" ).datepicker( {
				format: "dd-mm-yyyy"
			} );
			
			$( '.datetimepicker' ).datetimepicker( {
				format: "DD-MM-YYYY HH:mm"
			} );
		} );
		
		// set datepicker changed value to angular form field
		$( this._el_ref.nativeElement ).find( '.payable_invoice_due_date' ).on( 'change', ( e: any ) => {
			
			this.set_payable_invoice_due_date = e.target.value;
			
		} );
		
		// set datepicker changed value to angular form field
		$( this._el_ref.nativeElement ).find( '.receivable_invoice_due_date' ).on( 'change', ( e: any ) => {
			
			this.set_receivable_invoice_due_date = e.target.value;
			
		} );
		
	}
	
}
