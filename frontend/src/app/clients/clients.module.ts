import { NgModule } from '@angular/core';

/* import modules */
import { CommonModule } from '@angular/common';
import { ClientsRoutingModule } from './clients-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth } from 'angular2-jwt';
import { DatePickerModule } from 'ng2-datepicker';

/* import Component */
import { ClientsComponent } from './clients.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientCreateComponent } from './client-create/client-create.component';

import { ClientService } from './shared/client.service';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { ClientProjectListComponent } from './client-project-list/client-project-list.component';
import { ClientProjectCreateComponent } from './client-project-create/client-project-create.component';
import { ProjectService } from "../projects/shared/project.service";
import { ClientQuoteListComponent } from './client-quote-list/client-quote-list.component';
import { NotificationsService } from "angular2-notifications";
import { SimpleNotificationsModule } from 'angular2-notifications';

@NgModule( {
	imports     : [
		CommonModule,
		ClientsRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		DatePickerModule,
		SimpleNotificationsModule
	],
	declarations: [ ClientsComponent,
		ClientListComponent,
		ClientCreateComponent,
		ClientDetailComponent,
		ClientEditComponent,
		ClientProjectListComponent,
		ClientProjectCreateComponent,
		ClientQuoteListComponent ],
	providers   : [
		ClientService,
		ProjectService,
		AuthHttp,
		NotificationsService,
		/*provideAuth({
		 headerName: 'Authorization',
		 headerPrefix: 'bearer',
		 tokenName: 'token',
		 tokenGetter: (() => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2ptcy5sb2NhbDo4ODg4XC9hcGlcL2F1dGhlbnRpY2F0ZSIsImlhdCI6MTQ4OTgyNjkwNCwiZXhwIjoxNDg5ODMwNTA0LCJuYmYiOjE0ODk4MjY5MDQsImp0aSI6IjlkMThlNzIwMWI3NjYxZWEwOTJlZGQ3OTVlNDVmZjZmIn0.kyg_j1PY_jM0Z7ltQjoQHtPsOPgH1qy0zhlGgBf8HWk"),
		 //tokenGetter: (() => JSON.parse(localStorage.getItem('currentUser')).token),
		 globalHeaders: [{ 'Content-Type': 'application/json' }],
		 noJwtError: true
		 })*/
	],
} )
export class ClientsModule {
}
