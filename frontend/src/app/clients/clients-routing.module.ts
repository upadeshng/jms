import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

/* import components */
import {HomeComponent} from '../home/home.component';
import {ClientsComponent} from './clients.component';
import {ClientListComponent} from './client-list/client-list.component';
import {ClientCreateComponent} from './client-create/client-create.component';
import {ClientDetailComponent} from './client-detail/client-detail.component';
import {ClientEditComponent} from './client-edit/client-edit.component';
import {ClientProjectListComponent} from './client-project-list/client-project-list.component';
import {ClientQuoteListComponent} from './client-quote-list/client-quote-list.component';
import {ClientProjectCreateComponent} from './client-project-create/client-project-create.component';
import {AuthGuard} from "../auth-guard.service";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard],
        children: [
            {path: 'clients', component: ClientListComponent, canActivate: [AuthGuard]},
            {path: 'clients/create', component: ClientCreateComponent, canActivate: [AuthGuard]},
            { path: 'clients/:id', component: ClientDetailComponent , canActivate: [AuthGuard]},
            { path: 'clients/edit/:id', component: ClientEditComponent, canActivate: [AuthGuard] },
            { path: 'clients/projects/:id', component: ClientProjectListComponent, canActivate: [AuthGuard] },
            { path: 'clients/projects/create/:id', component: ClientProjectCreateComponent, canActivate: [AuthGuard] },
            { path: 'clients/quotes/:id', component: ClientQuoteListComponent, canActivate: [AuthGuard] },

        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ClientsRoutingModule {
}

//export const routedComponents = [ClientsComponent];