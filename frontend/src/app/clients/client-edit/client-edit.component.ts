import { Component, OnInit, NgZone, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ClientService } from "../shared/client.service";
import 'rxjs/add/operator/map';
import { ActivatedRoute, Router } from "@angular/router";
import { CustomValidators } from '../../shared/custom-validators';
import { Location } from '@angular/common';

// to use for jquery
declare var $: any;

@Component( {
	selector   : 'app-client-edit',
	templateUrl: './client-edit.component.html',
	styleUrls  : [ './client-edit.component.css' ]
} )
export class ClientEditComponent implements OnInit {
	
	clientForm: FormGroup;
	client: string;
	loading                                 = false;
	set_payable_invoice_due_date: string    = '';
	set_receivable_invoice_due_date: string = '';
	
	constructor( private _clientService: ClientService,
				 private _fb: FormBuilder,
				 private _route: ActivatedRoute,
				 private _router: Router,
				 private _location: Location,
				 private _el_ref: ElementRef,
				 // private detectorRef: ChangeDetectorRef,
				 private ngZone: NgZone ) {
		
	}
	
	go_back() {
		this._location.back();
	}
	
	ngOnInit() {
		
		
		
		// show loading message
		this.loading = true;
		
		// get client data
		
		this._load_client();
		
	}
	
	private _load_client() {
		this._route.params.subscribe( params => {
			
			let id = Number.parseInt( params[ 'id' ] );
			console.log( 'getting client with id: ', id );
			
			this._clientService.get( id )
				.map( res => res.json() )
				.subscribe(
					data => {
						this.client = data;
						
						this.clientForm = this._fb.group( {
							name                       : [ data.name, Validators.required ],
							address_line_1             : [ data.address_line_1 ],
							address_line_2             : [ data.address_line_2 ],
							state                      : [ data.state ],
							postcode                   : [ data.postcode ],
							phone                      : [ data.phone ],
							email                      : [ data.email,
								Validators.compose( [ CustomValidators.emailValidatorWithoutRequired ] ) ],
							payable_invoice_due_date   : [ data.payable_invoice_due_date ],
							receivable_invoice_due_date: [ data.receivable_invoice_due_date ],
							payment_terms              : [ data.payment_terms ],
						} );
						
						this.set_payable_invoice_due_date    = data.payable_invoice_due_date;
						this.set_receivable_invoice_due_date = data.receivable_invoice_due_date;
						
						console.log( data.address_line_1 );
					},
					err => {
						console.log( err );
					},
					() => {
						this.loading = false;
						console.log( 'Request Complete' )
					}
				);
			
		} );
	}
	
	on_update( e ): void {
		let id = Number.parseInt( this._route.params[ 'value' ][ 'id' ] );
		
		this._clientService.update( this.clientForm.value, id )
			.map( res => res.json() )
			.subscribe(
				data => {
					this._router.navigate( [ '/clients/' + id ] );
					//console.log(data);
					//this.go_back();
				},
				err => {
					console.log( err );
				},
				() => console.log( 'Request Complete' )
			);
	}
	
	ngAfterViewInit() {
		
	}
	
	ngAfterViewChecked() {
		let zone = this.ngZone;
		$( function () {
			
			$( "#datepicker-payable" ).datepicker( {
				format: "dd-mm-yyyy",
			} );
			
			$( "#datepicker-receivable" ).datepicker( {
				format: "dd-mm-yyyy"
			} );
			
		} );
		
		// set datepicker changed value to angular form field
		$( this._el_ref.nativeElement ).find( '.payable_invoice_due_date' ).on( 'change', ( e: any ) => {
			
			this.set_payable_invoice_due_date = e.target.value;
			
		} );
		
		// set datepicker changed value to angular form field
		$( this._el_ref.nativeElement ).find( '.receivable_invoice_due_date' ).on( 'change', ( e: any ) => {
			
			this.set_receivable_invoice_due_date = e.target.value;
			
		} );
		
	}
	
}
