import {Component, OnInit, ElementRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ClientService} from "../shared/client.service";
import 'rxjs/add/operator/map';
import {ActivatedRoute} from "@angular/router";
import {CustomValidators} from '../../shared/custom-validators';
import {Location} from '@angular/common';

// to use for jquery
declare var $: any;

@Component({
    selector: 'app-client-edit',
    templateUrl: './client-edit.component.html',
    styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

    clientForm: FormGroup;
    client: string;
    loading = false;

    constructor(private _clientService: ClientService,
                private _fb: FormBuilder,
                private _route: ActivatedRoute,
                private _location: Location,
                private _el_ref: ElementRef) {

        this.clientForm = _fb.group({
            name: ['', Validators.required],
            address_line_1: [''],
            address_line_2: [''],
            state: [''],
            postcode: [''],
            phone: [''],
            email: ['',
                Validators.compose([CustomValidators.emailValidatorWithoutRequired])],
            payable_invoice_due_date: [''],
            receivable_invoice_due_date: [''],
        });
    }

    go_back() {
        this._location.back();
    }

    ngOnInit() {
        // show loading message
        this.loading = true;

        // get client data
        this._route.params.subscribe(params => {

            let id = Number.parseInt(params['id']);
            console.log('getting client with id: ', id);

            this._clientService.get(id)
                .map(res => res.json())
                .subscribe(
                    data => {
                        this.client = data;
                        console.log(data.address_line_1);
                    },
                    err => {
                        console.log(err);
                    },
                    () => {
                        this.loading = false;
                        console.log('Request Complete')
                    }
                );

        });
    }

    on_update(e): void {
        let id = Number.parseInt(this._route.params['value']['id']);

        this._clientService.update(this.clientForm.value, id)
            .map(res => res.json())
            .subscribe(
                data => {
                    console.log(data);
                    this.go_back();
                },
                err => {
                    console.log(err);
                },
                () => console.log('Request Complete')
            );
    }

    ngAfterViewInit() {

        $(function() {
            $(".datepicker").datepicker({
                format: "dd-mm-yyyy"
            });

            $('.datetimepicker').datetimepicker({
                format: "DD-MM-YYYY HH:mm"
            });
        });


    }
}
