import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

export const routes: Routes = [
	{ path: '', component: HomeComponent },
	//{ path: '', redirectTo: 'vdvv', pathMatch: 'full'},
	//{ path: '', pathMatch: 'full', redirectTo: 'aaaa' },
	{ path: 'login', component: LoginComponent },
	{ path: 'logout', component: LogoutComponent },
	{ path: '**', redirectTo: '', pathMatch: 'full' },
//{ path: 'list', loadChildren: 'app/users/users.module#UsersModule' },
	//{ path: 'abc', loadChildren: 'app/users/users.module#UsersModule' }
];

@NgModule( {
	imports: [ RouterModule.forRoot( routes ) ],
	exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
